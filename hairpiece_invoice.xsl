<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <!-- this is the default namespace for stylesheet -->
 
<xsl:output method="html" indent="yes" encoding="utf-8" />
 <xsl:template match="/">
 
  <html lang="en">
   <head>
   <title>Hairpiece Invoice</title>
   <style type="text/css">
    table{
      border: 1px solid #000;
      border-collapse: collapse;
      width: 500px;
    }
    th, td{
      border: 1px solid #000;
      padding: 5px;
    }
    
    th{
      color:#fff;
      background: #333;
    }
   </style>
   </head>
   <body>
   <h1>Hairpiece Invoice</h1>
   
   <table>
   <xsl:for-each select="invoice" >
    <tr>
     <th>Date</th>
     <td><xsl:value-of select="date" /></td>
    </tr>
    <tr>
     <th>Customer</th>
     <td><xsl:value-of select="customer" /></td>
    </tr>
    <tr>
     <th>Client</th>
     <td><xsl:value-of select="client" /></td>
    </tr>
    <tr>
     <th>amount</th>
     <td><xsl:value-of select="amount" /></td>
    </tr>
    <tr>
     <th>Hairpiece</th>
     <td>
      <ul>
       <xsl:for-each select="hairpiece_list/hairpiece">
        <li>
         <xsl:value-of select="." />
        </li>
       </xsl:for-each>
      </ul>
     </td>
    </tr>
    <tr>
     <th>Status</th>
     <td><xsl:value-of select="status/@active" />,</td>
    </tr>
    <tr>
     <th>Description</th>
     <td><xsl:value-of select="description" /></td>
    </tr>
   </xsl:for-each>
   </table>
   </body>
  </html>
 </xsl:template>
 
</xsl:stylesheet>