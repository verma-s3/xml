<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <!-- this is the default namespace for stylesheet -->
 
<xsl:output method="html" indent="yes" encoding="utf-8" />
 <xsl:template match="/">
  
  <html lang="en">
   <head>
    <title>Planets</title>
    <style>
      table{
        border: 1px solid #000;
        border-collapse: collapse;
      }
      
      th, td{
        border: 1px solid #000;
        padding: 5px 10px;
      }
      
      th{
       color: #fff;
       background: #111;
      }
      
      tr:nth-child(even){
        background: #ccc;
      }
      
      tr:nth-child(odd){
        background: #eee;
      }
      
      tr + tr > th{
        background: #500;
        border-bottom: 1px solid #fff;
      }
      
      tr + tr > th:last-child{
        border-bottom: 1px solid #000;
      }
      
    </style>
   </head>
   <body>
   <xsl:variable name="my_link" select="planets/link_address" />
   <p>
    <a href="{$my_link}"><xsl:value-of select="planets/link_text" /></a>
   </p>
   
   <table>
   <tr>
    <th>Planet Name</th>
    <th>Moons</th>
    <th>Diameter</th>
    <th>Year Length</th>
    <th>Day Length</th>
   </tr>
   
   <xsl:for-each select="planets/planet" >
   <xsl:sort select="@position"  />
   <tr>
    <th><xsl:value-of select="name" /></th>
    <td>
     <xsl:choose>
      <xsl:when test="moons='None'" >
       <xsl:value-of select="moons" />
      </xsl:when>
      <xsl:otherwise>
       <ul>
        <xsl:for-each select="moons/moon" >
         <li><xsl:value-of select="." /></li>
        </xsl:for-each>
       </ul>
      </xsl:otherwise>
     </xsl:choose>
    </td>
    <td><xsl:value-of select="diameter" /></td>
    <td><xsl:value-of select="year_length" /></td>
    <td><xsl:value-of select="day_length" /></td>
   </tr>
   </xsl:for-each>
   </table>
   </body>
  </html>
 </xsl:template>
</xsl:stylesheet>