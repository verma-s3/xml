<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <!-- this is the default namespace for stylesheet -->
 
<xsl:output method="html" indent="yes" encoding="utf-8" />
 <xsl:template match="/">
 
  <html lang="en">
   <body>
   <img src="{image/src}" />
   </body>
  </html>
 </xsl:template>
</xsl:stylesheet>