<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <!-- this is the default namespace for stylesheet -->
 
 <xsl:output method="html" indent="yes" encoding="utf-8" />
 
 <xsl:template match="/">
  <!-- <h1>Is this working?</h1> -->
  <html lang="en">
   <head>
    <title>First XSL Document</title>
    <style type="text/css">
    
     body{
      font-family:Tahoma, sans-serif;
      font-size: 16px;
     }
     
     table{
      border: 1px solid #000;
      border-collapse: collapse;
      width: 600px;
     }
     
     td,th{
      border: 1px Solid #444;
      padding: 6px;
      vertical-align: top;
     }
     
     th{
      background: #333;
      color: #fff;
     }
     td{
      background: #ffc;
     }
     
     .smalltext{
       font-size: 0.7em;
     }
    </style>
   </head>
   
   <body>
    <h1>List of CD's - XSL </h1>
    <!-- Start looping
    <xsl:for-each select="cd_info/cd">
    <h2>
     <xsl:value-of select="/cd_info/cd/title" />  Absolute path
     <xsl:value-of select="title" />  Relative path
    </h2>
    </xsl:for-each>
    Stop looping -->
    
    
    <table>
     <tr>
     <th>Title</th>
     <th>Artist</th>
     <th>Songs</th>
     </tr>
     <xsl:apply-templates select="cd_info/cd">
     <xsl:sort select="title" order="descending" />
     </xsl:apply-templates>
    </table>
   </body>
  </html>
 </xsl:template>
 <!-- Extra template go here -->
 <xsl:template match="cd">
 <tr>
  <td>
   <xsl:value-of select="title" />
   <xsl:apply-templates select="year" />
  </td>
  <td><xsl:value-of select="artist" /></td>
  <td>
   <ul>
    <xsl:apply-templates select="song" />
   </ul>
  </td>
 </tr>
 
 </xsl:template>
 
 <xsl:template match="song">
   <li><xsl:value-of select="." /> </li>
 </xsl:template>
 
 <xsl:template match="year">
  <em class="smalltext">(<xsl:value-of select="." />)</em>
 </xsl:template>
 
</xsl:stylesheet>






















