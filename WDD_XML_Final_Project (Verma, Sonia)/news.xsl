<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <!-- this is the default namespace for stylesheet -->
 
<xsl:output method="html" indent="yes" encoding="utf-8" />
 <xsl:template match="/"><!-- template start -->
 
  <html lang="en">
  
   <head>
   <title>SSA World News</title>
   <!-- External CSS file -->
   <link rel="stylesheet" type="text/css" href="news.css" />
   
   </head>
   
   <body><!-- body start -->
   
    <header><!-- header start -->
     <div><xsl:value-of select="news_channel/header" /></div>
    </header><!-- header end -->
    
    <nav><!-- navigation menu start -->
     <div id="menu">
      <ul>
       <xsl:for-each select="news_channel/navigation/menu" ><!-- looping start -->
        <li class="{./@class}"><a href="#"><xsl:value-of select="." /></a></li>
       </xsl:for-each><!-- looping end -->
      </ul>
     </div>
    </nav><!-- navigation menu end -->
    
    <div id="container"><!-- container start --> 
     <div id="inner"><!-- inner start --> 
     
      <div id="main"><!-- main start --> 
       <div id="cover_pic"><!-- image div start -->
        <img src="{news_channel/image/src}" 
             alt="{news_channel/image/alt}"
             title="{news_channel/image/title}" />
       </div><!-- image div end -->
       <div><!-- Starting of content div -->
        <h1><span><xsl:value-of select="news_channel/contents/heading" /></span></h1>
        <!-- looping start -->
        <xsl:for-each select="news_channel/contents/paragraph" >
         <p><xsl:value-of select="." /></p>
        </xsl:for-each><!-- looping end -->
       </div><!-- ending of content div -->
       
       <div id="news_panel"><!-- news_panel start -->
        <h1>
         <span>
          <xsl:value-of select="news_channel/news_info/heading" />
         </span>
        </h1>
        <xsl:for-each select="news_channel/news_info/news" ><!-- looping start -->
        <xsl:sort select="date" order="descending"/>
         <div id="news_box">
          <h1>
          <a href="{link}"
             title="click_me">
           <xsl:value-of select="title" />
          </a> 
          </h1>
          <div>
           <img src="{image/src}" alt="{image/alt}" title="{image/title}" 
                style="float:right; margin-left: 10px; border-radius: 10px;" />
          </div>
          <p style="text-align:justify; font-size: 0.9em;">
           <xsl:value-of select="intro" />
          </p>
          <p style="color: #009;margin: 0;">
           <a href="{link}"
             title="click_me">
            <xsl:value-of select="link_text" />
           </a>
          </p>
          <p class="smalltext">
           (<xsl:value-of select="link" />)
          </p>
          <p style="background: #FF4500; color: #fff; text-align: right; 
             clear: both; padding-right: 20px;">
           <xsl:value-of select="date" />
          </p>
         </div>
        </xsl:for-each><!-- looping end -->
       </div><!-- news_panel end -->
       
      </div><!-- main end -->
      
      <div id="secondary"><!-- secondary start --> 
       <p>
        <span style="margin-bottom: 8px; display:block;">
         <xsl:value-of select="news_channel/side_bar/search" />
        </span>
        <input type="text" name="search_area" id="search_area" />
       </p>
       <div id="channels_list"><!-- channels_list start -->
        <p><xsl:value-of select="news_channel/side_bar/channel_info/channel" /></p>
        <ul>
         <xsl:for-each select="news_channel/side_bar/channel_info/channel_list" ><!-- looping start -->
          <li>
           <a href="{list_address}" title="click me">
            <xsl:value-of select="list_text" />
           </a><br />
           <em class="smalltext">(<xsl:value-of select="list_address" />)</em>
           <br />
          </li>
         </xsl:for-each><!-- looping end -->
        </ul>
       </div><!-- channels_list end -->
       <div id="categories"><!-- categories start -->
        <p><xsl:value-of select="news_channel/side_bar/Categories/news_category" /></p>
        <ul>
         <xsl:for-each select="news_channel/side_bar/Categories/category">
          <li><xsl:value-of select="." /></li>
         </xsl:for-each><!-- looping end -->
        </ul>
       </div><!-- categories end -->
      </div><!-- secondary end -->
      
     </div><!-- inner end -->
    </div><!-- container end -->
    
    <footer>
     <div><xsl:value-of select="news_channel/footer" /></div>
    </footer>
    
   </body><!-- body end -->
  </html>
 </xsl:template><!-- template end-->
</xsl:stylesheet><!-- stylesheet end -->