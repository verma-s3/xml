<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:t="http://www.brentscott.com/ns">
 <!-- this is the default namespace for stylesheet -->
 
 <xsl:output method="html" indent="yes" encoding="utf-8" />
 
 <xsl:template match="/">
  <!-- <h1>Is this working?</h1> -->
  <html lang="en">
   <head>
    <title>First XSL Document</title>
    <style type="text/css">
    
     body{
      font-family:Tahoma, sans-serif;
      font-size: 16px;
     }
     
     table{
      border: 1px solid #000;
      border-collapse: collapse;
      width: 600px;
     }
     
     td,th{
      border: 1px Solid #444;
      padding: 6px;
      vertical-align: top;
     }
     
     th{
      background: #333;
      color: #fff;
     }
     
     .smalltext{
      font-size:0.7em;
     }
     
     .darkgreen{
      color: #0a0;
     }
     
     .darkred{
       color: #a00;
     }
    </style>
   </head>
   
   <body>
    <h1>List of CD's - XSL </h1>
    <!-- Start looping
    <xsl:for-each select="cd_info/cd">
    <h2>
     <xsl:value-of select="/cd_info/cd/title" />  Absolute path
     <xsl:value-of select="title" />  Relative path
    </h2>
    </xsl:for-each>
    Stop looping -->
    <xsl:variable name="my_link" select="t:cd_info/t:link_url" />
    <p>
    <a href="{$my_link}"><xsl:value-of select="t:cd_info/t:link_text" /></a>
    <em class="smalltext">(<xsl:value-of select="$my_link" />)</em>
    </p>
    <table>
     <tr>
     <th>Title</th>
     <th>Artist</th>
     <th>Songs</th>
     </tr>
     
     <!-- Start looping -->
     <xsl:for-each select="t:cd_info/t:cd">
      <!-- Any Sorting much occur right after the for-each loop -->
      <xsl:sort select="t:artist" order="descending" />
      <xsl:sort select="t:title"  />
      <!-- perform a test  to check a year -->
      <xsl:if test="t:year != 2002">
       <tr>
        <td>
         <xsl:choose>
          <xsl:when test="t:title/@remastered='yes'">
           <span class="darkgreen"><xsl:value-of select="t:title" /></span>
          </xsl:when>
          <xsl:otherwise>
           <span class="darkred"><xsl:value-of select="t:title" /></span> 
          </xsl:otherwise>
         </xsl:choose>
         
         &#160;
         <em class="smalltext">(<xsl:value-of select="t:year" />)</em><br />
         Remastered:<xsl:value-of select="t:title/@remastered" />
        </td>
        <td><xsl:value-of select="t:artist" /></td>
        <td>
         <ul>
          <!-- Start looping -->
          <xsl:for-each select="t:song">
          <!-- Here, we cannot use song as the node, beacause we are
               already inside the song node which is our current node. 
               using "Song" inside would imply that there is a "song" element 
               within another "Song element". Therefore, we use the characters
               that refers to our current context node, the dot("."). This is 
               similar to how directories are accessed through terminal/ 
               command methods. " -->
           <li><xsl:value-of select="." /></li>
          </xsl:for-each>
          <!-- Stop looping -->
         </ul>
        </td>
       </tr>
      </xsl:if>
     </xsl:for-each>
     <!-- Stop looping -->
    </table>
   </body>
  </html>
 </xsl:template>
 
</xsl:stylesheet>