<?xml version="1.0" encoding="utf-8" ?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <!-- this is the default namespace for stylesheet -->
 
<xsl:output method="html" indent="yes" encoding="utf-8" />
 <xsl:template match="/">
 
  <html lang="en">
   <body>
   <table>
   <xsl:for-each select="channel_info/channel_list">
    <tr>
      <td><xsl:value-of select="list_text"/></td>
      <td><xsl:value-of select="list_address"/></td>
    </tr>
    </xsl:for-each>
   </table>
   </body>
  </html>
 </xsl:template>
</xsl:stylesheet>